## 1. Ausgangssituation

Datengetriebe Forschung gewinnt immer weiter an Bedeutung - doch wie mit diesen Daten umgehen?


### Ausgangssituation: Daten gewinnen mehr und mehr Bedeutung in Forschung und müssen entsprechend behandelt werden

* Forschung, nicht zuletzt auch in den Ingenieurwissenschaften, wird immer **datengetriebener**
* Forschende möchten sich typischerweise mit Ihrem Fachgebiet vertiefen, aber sich **nicht unbedingt in das Datenmanagement einarbeiten**
* **Forschungsdatenmanagement (FDM)** als Querschnittsfunktion kann hier Abhilfe schaffen, indem es **wiederverwendbare Angebote und Lösungen entwickelt und bereitstellt**.
    * Auswahl von Tools und Repositorien
    * Hinweis auf notwendige Schritte und Best Practices
    * Bereitstellung von Trainingsmaterialien
    * Empfehlungen zu spezifischen Situationen und Problemstellungen, z.B. Umgang mit vielfältigen Daten oder großen Datenmengen



## 2. Potenziale der Datennachnutzung

Datennachnutzung bietet Potenziale und neue Möglichkeiten für die eigene Forschung, erfordert aber entsprechende Aufbereitung


### Datennachnutzung eröffnet neue Möglichkeiten für Forschung

* Anstatt eigene Daten zu erheben besteht grundsätzlich die Möglichkeit, **bestehende Daten nachzunutzen**
    * Geringer bis kein Aufwand für Datenerhebung (aber Aufwand für Einarbeitung), sodass Zeit und Kosten gespart werden
    * An bestehenden Daten können weitere Aspekte erforscht werden
    * Nicht immer lassen sich Daten selbst erheben, etwa weil zu teuer (z.B. Raketenstart, Werkzeugmaschine, Teilchenbeschleuniger)
* Unter **Berücksichtigung von ethischen, gesetzlichen und sozialen Aspekten** (ethical, legal and social aspects, kurz "ELSA")
* Stellt die **Herausforderungen**, Daten zu finden und nachnutzen zu können
    * FAIR Prinzipien
    * Datenqualität
    * Aufbereitung und Dokumentation der Daten


### Doch dies funktioniert typischerweise nicht 'einfach so' ...

[![Cartoon](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/AukeHerrema2014_DataForFutureGeneration_0_33.png)](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/AukeHerrema2014_DataForFutureGenerations.png)

Cartoon "Data for future generations" by Auke Herrema – Het Bouwteam under CC-BY license at [https://www.fosteropenscience.eu/content/cartoondata-future-generations](https://www.fosteropenscience.eu/content/cartoondata-future-generations)



## 3. Gute wissenschaftliche Praxis (GwP)

Grundsätzliche Leitlinien für Forschungsprozesse und damit auch den Umgang mit Forschungsdaten


### Gute wissenschaftliche Praxis (GwP) stellt grundsätzliche Leitlinien für Forschungsprozesse und damit auch den Umgang mit Forschungsdaten auf

* Grundlage, um Fördermittel durch die DFP zu erhalten
* Grundsätzliche Prinzipien sowie zum Forschungsprozess
* Qualitätssicherung zur Forschung und Veröffentlichung
* Damit auch: Nachvollziehbare Dokumentation der Forschung
* Leitlinien mit hohem Abstraktionsgrad, der durch fachspezifische Erläuterungen sowie detaillierte, fachspezifische Ausführungen konkretisiert wird _(vgl. Abbildung)_

[![Struktur GwP](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/GwP_0_75.png "Struktur GwP")*Abbildung: Struktur des Kodex "Leitfaden zur Sicherstellung guter wissenschaftlicher Praxis"*](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/GwP.png) [[Quelle]](https://doi.org/10.5281/zenodo.6472827)


### Quelle und Literaturempfehlung zur GwP 

* [https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/gwp/](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/gwp/)
* [DFG: Leitlinien zur Sicherstellung guter wissenschaftlicher Praxis (Kodex). Stand: April 2022 / korrigierte Version 1.1](https://doi.org/10.5281/zenodo.6472827) 
* Übersichtsseite: [Fachspezifische Empfehlungen der DFG zum Umgang mit Forschungsdaten](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/empfehlungen/index.html) (für Ingenieurwissenschaften aktuell Materialwissenschaften und Werkstofftechnik)



## 4. Weitere Anforderungen von Forschungsförderern

Neben der intrinsischen Motivation und der GwP kann FDM auch eine Forderung von Forschungsförderern sein


### Forschungsdatenmanagement kann von den Forschungsförderern gefordert werden

* Forschungsförderer zielen auf die Bereitstellung und Nachnutzung von erhobenen Daten ab, um **Transparenz** in den Ergebnissen herzustellen und eine mögliche **Nachnutzung** zu ermöglichen
* Zukünftig weitere Entwicklung zu erwarten: FDM kann zukünftig in allen Forschungsprojekten erforderlich werden
* Beispielhafte Ausschreibungsbedingungen in _Abbildung_

[![Beispiel für FDM-Anforderungen in einem Forschungsprojekt](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/AnforderungFF_75.png "Beispiele für FDM-Anforderungen in einem Forschungsprojekt")*Abbildung: Beispiel für FDM-Anforderungen in einem Forschungsprojekt*](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/AnforderungFF.png) [Quelle: Eigene Darstellung, [Bekanntmachung des Bundesministerium für Bildung und Forschung](https://www.bmbf.de/bmbf/shareddocs/bekanntmachungen/de/2021/08/2021-08-17-Bekanntmachung-Fachhochschulen.html)]

(Hinweis: Gegenstand dieses Forschungsprojektes ist das FDM an Fachhochschulen, sodass diese Anforderungen hier möglicherweise besonders umfangreich ausgefallen sind.)



## 5. FDM als Bestandteil zur Erfüllung der GwP

FDM kann ein Bestandteil dabei sein, die GwP im Umgang mit Daten zu erfüllen


### Wie kommen nun Forschungsdatenmanagement und GwP zusammen?

Forschungsdatenmanagement hilft mit seinen Methoden und Angeboten, Aspekte der guten wissenschaftlichen Praxis (Abbildung links) innerhalb des Forschungsdatenlebenszyklus (rechts) zu erfüllen!

[![Zuordnung zwischen Kapiteln der GwP und dem Datenlebenszyklus](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/GwP-DLC_75T.png "Zuordnung zwischen Kapiteln der GwP und dem Datenlebenszyklus")*Abbildung: Beispiel für FDM-Anforderungen in einem Forschungsprojekt*](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/GwP-DLC.png) [Quelle: Eigene Darstellung, [DFG: Leitlinien zur Sicherstellung guter wissenschaftlicher Praxis (Kodex)](https://doi.org/10.5281/zenodo.6472827), [DLC der RWTH Aachen](https://blog.rwth-aachen.de/forschungsdaten/2021/04/16/fdm-erklaert-was-ist-eigentlich-ein-daten-lebenszyklus/
)]



## 6. Vorbehalte

Auch wenn die Motivation für FDM deutlich geworden ist, so stehen dem Thema auch Vorbehalte gegenüber.


### Vorbehalte gegenüber FDM und wie diesen begegnet werden kann

FDM kann auch kritisch gesehen werden. Hier einige Vorbehalte und mögliche Lösungsansätze hierzu:

| Vorbehalt | Lösungsansätze hierzu |
| -------- | -------- |
| Mehraufwand | Ja, ist es zunächst einmal. Mehraufwand lässt sich bei Forschungsanträgen aber auch einpreisen, etwa für Personal, Zeit und Ressourcen (z.B. bei der [DFG](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/beantragbare_mittel/index.html)). Letztlich kann ein effektiveres Datenmanagement zu einer Zeitersparnis führen.  |
| Datenschutzbedenken, Veröffentlichung von personenbezogenen Daten, wissenschaftlicher Diebstahl | Anonymisierung, Lizensierung, ggf. Veröffentlichung unter Embargo |
| Fehlendes Bewusstsein oder fehlendes Wissen zu FDM | Nutzung von Ansprechpartnern, Beratungs- und Trainingsangeboten, z.B. Universitätsbibliotheken, Data Stewards, [NFDI4Ing](https://nfdi4ing.de), [forschungsdaten.info](https://www.forschungsdaten.info/), ... |
| Fehlende Standardisierung | Orientierung an Vorgaben, Organisationen sowie Best Practices |



## 7. Beispiele

In diesem Kapitel werden zwei positive und zwei negative Beispiele beleuchtet, die die vielfältigen Herausforderungen und Chancen des Datenmanagements verdeutlichen. 


### Forschung eines Archäologen

![Beispiel Archäologe](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/beispiel_archaeologe.png)


### Human Genome Project

![Beispiel Genome](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/beispiel_genom.png)


### Mars Climate Orbiter

![Beispiel MCO](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/beispiel_mars_orbiter.png)


### NEPTUNE Canada

![Beispiel Neptune Canada](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/rdm-motivation/-/raw/master/media_files/beispiel_neptune_canada.png)


### Quellen

Forschung eines Archäologen <br>

https://www.cambridge.org/core/services/aop-cambridge-core/content/view/DBEBC733DA7D5C381A59D6E70C5AB59D/S2326376817000377a.pdf/sociotechnical-obstacles-to-archaeological-data-reuse.pdf <br>

Human Genome Project <br>

https://science.howstuffworks.com/life/genetic/human-genome-project-results.htm <br>
https://www.genome.gov/about-genomics/educational-resources/fact-sheets/human-genome-project <br>
https://www.nature.com/articles/526029a <br>
https://www.ebi.ac.uk/about/news/perspectives/five-things-you-probably-didn-t-know-about-human-genome-project/ <br>
https://apps.dtic.mil/sti/tr/pdf/ADA337589.pdf <br>
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8490009/ <br>

Mars Climate Orbiter <br>
https://sma.nasa.gov/docs/default-source/safety-messages/safetymessage-2009-08-01-themarsclimateorbitermishap-vits.pdf?sfvrsn=7aa1ef8_4 <br>
https://sma.nasa.gov/docs/default-source/safety-messages/safetymessage-2009-08-01-themarsclimateorbitermishap.pdf?sfvrsn=eaa1ef8_4 <br>
https://www.swr.de/wissen/technik-fail-einheitenfehler-laesst-mars-climate-orbiter-zerschellen-100.html <br>
https://www.washingtonpost.com/wp-srv/national/longterm/space/stories/orbiter100199.htm <br>

NEPTUNE Canada <br>
https://www.researchgate.net/publication/285666930_The_NEPTUNE_Canada_regional_cabled_ocean_observatory <br>
https://www.oceannetworks.ca/ <br>
https://journals.lib.unb.ca/index.php/GC/article/download/18588/20203?inline=1 <br>

